import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.grey[900],
        title: Text("Asean Countries"),
      ),
      backgroundColor: Colors.grey[850],
      body: Center(
        child: SingleChildScrollView(
          child: 
            Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/indonesia-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("INDONESIA", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("JAKARTA", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
             Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/philippines-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("FILIPINA", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("MANILA", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/malaysia-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("MALAYSIA", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("KUALA LUMPUR", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/singapore-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("SINGAPORE", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("SINGAPORE", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/thailand-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("THAILAND", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("BANGKOK", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/brunei-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("BRUNEI", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("BANDAR SERI BEGAWAN", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/vietnam-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("VIETNAM", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("HANOI", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/laos-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("LAOS", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("VIENTIANE", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/myanmar-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("MYANMAR", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("NAYPYITAW", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
              Row(
              children: [
                Container(
                width: 120,
                height: 80,
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        'assets/img/cambodia-flag-small.png'),
                    fit: BoxFit.fill,
                  ),
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                
                ),
                Container(
                width: 220,
                height: 80,
               
                alignment: Alignment.center,
                margin: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.grey[800],
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  shape: BoxShape.rectangle,
                ), 
                 child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text("KAMBOJA", 
                  
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22, color: Colors.white),
                    
                    ), 
                    new Text("PHNOM PENH", 
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 18, color: Colors.white),  
                    ), 
                  ],),
                
                
                ),
              ],),
            
            ],
            
          )
        )
      ),
    );
  }
}


